from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import redirect, render

from .models import GroceryItem


# Create your views here.
def index(request):
    groceryItem_list = GroceryItem.objects.all()
    context = {
        'groceryItem_list': groceryItem_list
    }

    return render(request, 'groceryItems/index.html', context)

def groceryItem(request, groceryItem_id):
    groceryItem = model_to_dict(GroceryItem.objects.get(pk=groceryItem_id))
    return render(request, "groceryItems/groceryItem.html", groceryItem)

def registerUser(request):
    users = User.objects.all()
    is_user_registered = False

    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "mjwatson":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = "mjwatson"
        user.first_name = "Mary Jane"
        user.last_name = "Watson"
        user.email = "mjwatson@mail.com"
        user.set_password("mjwatson1234")
        user.is_staff = False
        user.is_active = True
        user.save()
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name,
        }
    return render(request, "groceryItems/register.html", context)


def change_password(request):

    is_user_authenticated = False
    user = authenticate(username="mjwatson", password="mjwatson1")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username="mjwatson")
        authenticated_user.set_password("mjwatson1234")
        authenticated_user.save()
        is_user_authenticated = True
        context = {
            "is_user_authenticated": is_user_authenticated
        }

        return render(request, "groceryItems/change_password.html", context)
    
    return redirect('logout')

def login_view(request):
    username = 'mjwatson'
    password = 'mjwatson1'
    user = authenticate(username=username, password=password)
    context = {
        'is_user_authenticated': False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect('index')
    else:
        return render(request, "groceryItems/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")