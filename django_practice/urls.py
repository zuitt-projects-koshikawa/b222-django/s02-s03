# make sure to include this folder when migrating.
from django.urls import path

from . import views

# path() function
# path(route, view, name)

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:groceryItem_id>/', views.groceryItem, name='groceryItem'),
    # /djangoPractice/register
    path('register', views.registerUser, name='register'),
    # /djangoPractice/change_password
    path('change_password', views.change_password, name='change_password'),
    # /djangoPractice/login
    path('login/', views.login_view, name='login'),
    # /djangoPractice/logout
    path('logout/', views.logout_view, name='logout')
    
]